import "./App.css";
import React, { useState } from "react";
import styled from "styled-components";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { data, columnTaskMap } from "./move-column-assets/data";
import reorder, { reorderTaskMap } from "./move-column-assets/reorder";
import { colors } from "@atlaskit/theme";

// console.log(columnTaskMap);


//constaint
const scrollContainterHeight = 250;
const grid = 8;


const getBgColor = (isDraggingOver, isDraggingFrom) => {
  if (isDraggingOver) {
    return colors.R50;
  }
  if (isDraggingFrom) {
    return colors.T50;
  }
  return colors.N30;
};

const TaskItem = styled.div`
  width: 200px;
  border: 1px solid grey;
  margin-bottom: 8px;
  background-color: ${(props) => (props.isDragging ? "pink" : "white")};
  padding: 8px;
  min-height: 40px;
  user-select: none;
  display: flex;
`;

const Title = styled.h4``;

const Wrapper = styled.div`
  background-color: ${(props) =>
    getBgColor(props.isDraggingOver, props.isDraggingFrom)};
  width: 250px;
  padding: ${grid}px;
  border: ${grid}px;
  padding-bottom: 0px;
  transition: background-color 0.2 ease;
  display: flex;
  flex-direction: column;
`;

const DropZone = styled.div`
    // min-height: 250px;
    // padding-bottom: ${grid}px;
    // max-height: ${scrollContainterHeight}px;
`;

const InnerListContainer = styled.div``;

const InnerList = React.memo(function InnerList(props) {
  return props.tasks.map((task, index) => {
    console.log(task.id);
    return (
      
      <InnerListContainer key={index}>
        <DropZone key={index}>
          <Draggable draggableId={task.id} key={index} index={index}>
            {(dragProvided, dragSnapshot) => (
              // <TaskItem key={task.id} task={task} isDragging={dragSnapshot.isDragging} provided={dragProvided}/>
              <TaskItem
                ref={dragProvided.innerRef}
                isDragging={dragSnapshot.isDragging}
                {...dragProvided.draggableProps}
                {...dragProvided.dragHandleProps}
              >
                {task.content}
              </TaskItem>
            )}
          </Draggable>
        </DropZone>
      </InnerListContainer>
    );
  });
});

//TaskList la droppable cho TaskItem
const TaskList = (props) => {
  // console.log(props);
  return (
    <Droppable droppableId={props.listId} type={props.listType}>
      {(provided, snapshot) => (
        <Wrapper
          isDraggingOver={snapshot.isDraggingOver}
          ref={provided.innerRef}
          isDraggingFrom={Boolean(snapshot.draggingFromThisWith)}
          {...provided.droppableProps}
        >
          <InnerList tasks={props.tasks} dropProvided={provided} />
          {provided.placeholder}
        </Wrapper>
      )}
    </Droppable>
  );
};

//Column la 1 draggable
const Column = (props) => {
  // console.log(props);
  return (
    <Draggable draggableId={props.title} index={props.index}>
      {(provided, snapshot) => (
        <ColumnContainer  ref={provided.innerRef} {...provided.draggableProps}>
          <Title {...provided.dragHandleProps}>{props.title}</Title>
          <TaskList listId={props.title} listType="TASK" tasks={props.tasks} />
        </ColumnContainer>
      )}
    </Draggable>
  );
};

function TaskApp() {
  // const [state, setState] = useState(() => data);
  const [columns, setColumns] = useState(() => columnTaskMap);
  const [ordered, setOrdered] = useState(() => data["columnOrder"]);
  function handleOnDragEnd(result) {
    if (result.combine) {
      if (result.type === "COLUMN") {
        const shallow = [...ordered];
        shallow.splice(result.source.index, 1);
        setOrdered(shallow);
        return;
      }

      const column = columns[result.source.droppableId];
      const withQuoteRemoved = [...column];
      withQuoteRemoved.splice(result.source.index, 1);
      const columnsForSetState = {
        ...columns,
        [result.source.droppableId]: withQuoteRemoved,
      };

      setColumns(columnsForSetState);
      return;
    }

    if (!result.destination) {
      return;
    }

    const { source, destination } = result;

    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index
    ) {
      return;
    }

    if (result.type === "COLUMN") {
      const orderedForSetState = reorder(
        ordered,
        source.index,
        destination.index
      );
      // console.log(ordered);
      setOrdered(orderedForSetState);
      return;
    }

    const data = reorderTaskMap({ quoteMap: columns, source, destination });

    setColumns(data.quoteMap);
  }

  //Board la 1 droppable cho column
  const board = (
    <Droppable droppableId="board" type="COLUMN" direction="horizontal">
      {(provided) => (
        <Container ref={provided.innerRef} {...provided.droppableProps}>
          {ordered.map((column, index) => {
            return (
              <Column
                key={column}
                index={index}
                title={column}
                tasks={columns[column]}
              />
            );
          })}
          {provided.placeholder}
        </Container>
      )}
    </Droppable>
  );

  return (
      <DragDropContext onDragEnd={handleOnDragEnd}>{board}</DragDropContext>
  );
}

const Container = styled.div`
  display: inline-flex;
  margin-bottom: 100px;
`;

const ColumnContainer = styled.div`
  margin: 30px
  background-color: red;
  display: flex;
  flex-direction: column;
`;

export default TaskApp;

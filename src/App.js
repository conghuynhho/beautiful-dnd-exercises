import "./App.css";
import React, { useState } from "react";
import styled from "styled-components";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

import initialData from "./initial-data";
import { move, reorder, checkDropAbility } from "./function";

const TaskItem = styled.div`
  width: 200px;
  border: 1px solid grey;
  margin-bottom: 8px;
  background-color: ${(props) => (props.isDragging ? "#FAE279" : "white")};
  padding: 8px;
`;

const getItemStyle = (isDragging, draggingOver, draggableStyles, taskId) => {
  handleOnDragging(isDragging, draggingOver, taskId);
  return {
    userSelect: "none",
    // backgroundColor:isDragging ? "lightgreen" : "grey",
    ...draggableStyles,
  };
};
const getListStyle = (isDraggingOver) => ({
  background: isDraggingOver ? "lightblue" : "lightgrey",
  width: 250,
  padding: 8,
});

const handleOnDragging = (isDragging, draggingOver, taskId) => {
  const [...droppableAreas] = document.querySelectorAll(
    "div[data-rbd-droppable-id]"
  );
  if (isDragging) {
    console.log("Dragging");
    console.log(taskId);
    // kiem tra neu dang drag over thi bgColor = green or red
    // neu khong drag over thi bgColor = darkgreen or drakred
    // neu  isDragging == false thi bgColor = lightgrey

    droppableAreas.map((area) => {
      let areaName = area.getAttribute("data-rbd-droppable-id");
      let ableDropArea = checkDropAbility(taskId);
      
      if (ableDropArea.includes(areaName)) {
        if (draggingOver === areaName) {
          setBgColorToList("#59d350", area);
        } else {
          setBgColorToList("#97ce94", area);
        }
      } else {
        if (draggingOver === areaName) {
          setBgColorToList("#dd373a", area);
        } else {
          setBgColorToList("#ea797b", area);
        }
      }
      return null;
    });
  } else {
    droppableAreas.map((area) => {
      setBgColorToList("lightgrey", area);
      return null;
    });
  }
};

const setBgColorToList = (color, listElement) => {
  if (listElement) {
    listElement.style.backgroundColor = color;
  }
};
function Task({ task, index }) {
  return (
    <Draggable draggableId={task.id} index={index}>
      {(provided, snapshot) => (
        <TaskItem
          isDragging={snapshot.isDragging}
          style={getItemStyle(
            snapshot.isDragging,
            snapshot.draggingOver,
            provided.draggableProps.style,
            provided.draggableProps["data-rbd-draggable-id"]
          )}
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          {task.content}
          {/* {console.log(snapshot, provided)} */}
        </TaskItem>
      )}
    </Draggable>
  );
}
// style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
// style={getListStyle(snapshot.isDraggingOver)}
const TaskList = React.memo(function TaskList({ tasks }) {
  if (!tasks) {
    console.log("Trong TaskList data tasks == undefined");
    return <div>Nothing here to see</div>;
  }
  return tasks.map((task, index) => (
    <Task task={task} index={index} key={task.id}></Task>
  ));
});

function App() {
  const [state, setState] = useState(initialData);

  function handleOnDragEnd(result) {
    const { source, destination } = result;
    if (!destination) {
      return;
    }
    if (source.droppableId === destination.droppableId) {
      const sourceID = source.droppableId;
      const newSource = reorder(
        state[source.droppableId],
        source.index,
        destination.index
      );
      const result = {};
      result[sourceID] = newSource;
      setState({ ...state, ...result });
    } else {
      const result = move(
        state[source.droppableId],
        state[destination.droppableId],
        source,
        destination
      );
      setState({ ...result });
    }
  }

  return (
    <div className="container">
      <div className="wrapper">
        <DragDropContext onDragEnd={handleOnDragEnd}>
          <Droppable droppableId="tasks">
            {(provided, snapshot) => (
              <div
                style={getListStyle(snapshot.isDraggingOver)}
                ref={provided.innerRef}
                {...provided.droppableProps}
              >
                <h2>{Object.keys(state)[0]}</h2>
                <TaskList tasks={state.tasks} />
                {provided.placeholder}
              </div>
            )}
          </Droppable>
          <Droppable droppableId="done">
            {(provided, snapshot) => (
              <div
                style={getListStyle(snapshot.isDraggingOver)}
                ref={provided.innerRef}
                {...provided.droppableProps}
              >
                <h2>{Object.keys(state)[1]}</h2>
                <TaskList tasks={state.done} />
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </div>
      <br />
      <hr />
    </div>
  );
}

export default App;
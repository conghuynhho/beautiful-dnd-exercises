import React, {useState} from 'react'
import {Droppable, Draggable, DragDropContext} from 'react-beautiful-dnd'
import styled from 'styled-components'




const initialData = {
  tasks: [
    {id: 'task-1', content: 'task 1'},
    {id: 'task-2', content: 'task 2'},
    {id: 'task-3', content: 'task 3'},
    {id: 'task-4', content: 'task 4'},
    {id: 'task-5', content: 'task 5'},
    {id: 'task-6', content: 'task 6'},
    {id: 'task-7', content: 'task 7'},
    {id: 'task-8', content: 'task 8'},
  ]
}

const TaskItem = styled.div`
  width: 200px;
  border: 1px solid black;
  margin-bottom: 8px;
  padding: 8px;
`;

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0 , removed);

  return result;
}

function Task({task, index}){
  return(
    <Draggable draggableId = {task.id} index = {index}>
      {(provided) => (
        <TaskItem ref={provided.innerRef} 
        {...provided.draggableProps}
        {...provided.dragHandleProps}
        >
          {task.content}
        </TaskItem>
      )}
    </Draggable>
  );
}


const TaskList = React.memo( function TaskList({tasks}){
  return (
    tasks.map((task,index) => (
      <Task task = {task} index = {index} key = {task.id}></Task>
    ))
  );
});



function VerticalList(){
  const [state, setState] = useState(initialData);

  function handleOnDragEnd(result){
    if(!result.destination){
      return;
    }

    if(result.destination.index === result.source.index){
      return;
    }

    const tasks = reorder(state.tasks, result.source.index, result.destination.index);

    setState({tasks});
  }
  return(
    <DragDropContext onDragEnd = {handleOnDragEnd} >
      <Droppable droppableId = "list2">
        {(provided) => (
          <div ref = {provided.innerRef} {...provided.droppableProps}>
            <TaskList tasks = {state.tasks}/>
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
  
}


export default VerticalList;
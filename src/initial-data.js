
const initialData = {
  tasks: [
    { id: "task-1", content: "Task-1" },
    { id: "task-2", content: "Task-2" },
    { id: "task-3", content: "Task-3" },
    { id: "task-4", content: "Task-4" },
    { id: "task-5", content: "Task-5" },
    { id: "task-6", content: "Task-6" },
    { id: "task-7", content: "Task-7" },
    { id: "task-8", content: "Task-8" },
  ],
  done: [],
};

export default initialData;
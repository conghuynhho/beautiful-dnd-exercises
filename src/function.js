
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const move = (source, destination, droppableSource, droppableDestination) => {
  const newSource = Array.from(source);
  const newDestination = Array.from(destination);
  const [removed] = newSource.splice(droppableSource.index, 1);
  newDestination.splice(droppableDestination.index, 0, removed); // sử dụng hàm filter
  const result = {};
  result[droppableDestination.droppableId] = newDestination;
  result[droppableSource.droppableId] = newSource;
  return result;
};

// const advancedMove = (
//   source,
//   destination,
//   droppableSource,
//   droppableDestination
// ) => {
//   const newSource = Array.from(source);
//   const newDestination = Array.from(destination);
//   const addItem = newSource[droppableSource.index];
//   newDestination.splice(droppableDestination.index, 0, addItem);
//   const result = {};
//   result[droppableDestination.droppableId] = newDestination;
//   result[droppableSource.droppableId] = newSource;

//   return result;
// };

const taskName = {
  task_1: 'task-1',
  task_2: 'task-2',
  task_3: 'task-3',
  task_4: 'task-4',
  task_5: 'task-5',
  task_6: 'task-6',
  task_7: 'task-7',
  task_8: 'task-8',

} // camelCase 
// task-name, task_name, taskname
const checkDropAbility = (taskId) => {
  var result = [];
  switch (taskId) {
    case taskName.task_1: // khong dung string de so sanh
      result = ["done", "tasks"];
      break;
    case "task-2":
      result = ["done", "tasks"];
      break;
    case "task-3":
      result = ["done", "tasks"];
      break;
    case "task-4":
      result = ["done", "tasks"];
      break;
    case "task-5":
      result = ["done", "tasks"];
      break;
    case "task-6":
      result = ["done", "tasks"];
      break;
    case "task-7":
      result = ["tasks"];
      break;
    case "task-8":
      result = ["tasks"];
      break;
    default:
      break;
  }
  return result;
};

export {reorder, move, checkDropAbility};